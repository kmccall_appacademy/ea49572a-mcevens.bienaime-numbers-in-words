class Fixnum

def in_words
  case
  when self < 20
    return read_figure(self)
  when self < 100
    return read_tens(self)
  when self < 1000
    return read_hundred(self)
  when self < 10_000_000
    return read_thousand(self)
  when self < 1_000_000_000
    return read_millions(self)
  when self < 1_000_000_000_000

    return read_billions(self)
  when self < 1_000_000_000_000_000

    return read_trillions(self)
  end
end




private
def read_figure(input)
   case input
   when 0
     return "zero"
   when 1
     return "one"
   when 2
     return "two"
   when 3
     return "three"
   when 4
     return "four"
   when 5
     return "five"
   when 6
     return "six"
   when 7
     return "seven"
   when 8
     return "eight"
   when 9
     return "nine"
   when 10
    return "ten"
   when 11
      return "eleven"
   when 12
      return "twelve"
  when 13
       return "thirteen"
   when 14
        return "fourteen"
  when 15
     return "fifteen"
  when 16
      return "sixteen"
  when 17
       return "seventeen"
  when 18
      return "eighteen"
  when 19
     return "nineteen"
  end
end

def read_tens(input)
  return ten_word(input) if input % 10 == 0
  firstletter = input /10
  remains = input - (firstletter * 10)
  ten_word(firstletter * 10) + " " + remains.in_words
end

def read_hundred(input)
    firstletter = input/100
    remains = input - firstletter * 100
    firstletter.in_words + " " + "hundred" +  (remains == 0 ? "" : " " + remains.in_words)
end

def read_thousand(input)
  firstletter = input/1000
  remains = input -  firstletter * 1000
  firstletter.in_words + " " + "thousand" +  (remains == 0 ? "" : " " + remains.in_words)
end

def read_millions(input)
  firstletter = input/1000000
  remains = input - firstletter * 1000000
  firstletter.in_words + " " + "million" +  (remains == 0 ? "" : " " + remains.in_words)
end


def read_billions(input)
  firstletter = input/1000000000
  remains = input - firstletter * 1000000000
  firstletter.in_words + " " + "billion" +  (remains == 0 ? "" : " " + remains.in_words)
end

def read_trillions(input)
  firstletter = input/1000000000000
  remains = input - firstletter * 1000000000000
  firstletter.in_words + " " + "trillion" +   (remains == 0 ? "" : " " + remains.in_words)
end


def ten_word(input)
  case input
  when 20
    return "twenty"
  when 30
    return "thirty"
  when 40
    return "forty"
  when 50
    return "fifty"
  when 60
    return "sixty"
  when 70
    return "seventy"
  when 80
    return "eighty"
  when 90
    return "ninety"
  end
end



end
